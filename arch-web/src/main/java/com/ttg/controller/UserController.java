package com.ttg.controller;

//~--- non-JDK imports --------------------------------------------------------

import com.ttg.domain.User;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//~--- classes ----------------------------------------------------------------

/**
 * Class UserController
 * Description
 * Create 2016-02-16 16:45:30
 * @author Ardy    
 */
@EnableAutoConfiguration
@RestController
@RequestMapping("/user")
public class UserController {

    /**
     * Method view
     * Description
     * CreateDate 2016-02-16 16:45:30
     *
     * @param id Long
     *
     * @return User
     */
    @RequestMapping("/{id}")
    public User view(@PathVariable("id") Long id) {
        User user = new User();

        user.setId(id);
        user.setName("zhang");

        return user;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
