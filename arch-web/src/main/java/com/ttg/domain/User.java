package com.ttg.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Class User
 * Description
 * Create 2016-02-18 09:40:13
 * @author Ardy    
 */
@Entity
public class User implements Serializable{

    /** 
     * Field id
     * Description 
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /** 
     * Field name
     * Description 
     */
    private String name;

    /**
     * Method getId
     * Description
     * CreateDate 2016-02-18 09:40:13
     *
     * @return Long
     */
    public Long getId() {
        return id;
    }

    /**
     * Method setId
     * Description
     * CreateDate 2016-02-18 09:40:13
     *
     * @param id Long
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Method getName
     * Description
     * CreateDate 2016-02-18 09:40:13
     *
     * @return String
     */
    public String getName() {
        return name;
    }

    /**
     * Method setName
     * Description
     * CreateDate 2016-02-18 09:40:13
     *
     * @param name String
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Method equals
     * Description
     * CreateDate 2016-02-18 09:40:13
     *
     * @param o Object
     *
     * @return boolean
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if ((o == null) || (getClass() != o.getClass())) {
            return false;
        }

        User user = (User) o;

        if ((id != null) ? !id.equals(user.id) : user.id != null) {
            return false;
        }

        return true;
    }

    /**
     * Method hashCode
     * Description
     * CreateDate 2016-02-18 09:40:13
     *
     * @return int
     */
    @Override
    public int hashCode() {
        return (id != null) ? id.hashCode() : 0;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
