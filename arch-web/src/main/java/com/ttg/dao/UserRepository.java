package com.ttg.dao;

import com.ttg.domain.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Administrator
 *
 * @date 2016/2/18.
 */
@Repository
public interface UserRepository  extends CrudRepository<User,Long>{
    @Override
    <S extends User> S save (S s);

    @Override
    <S extends User> Iterable<S> save (Iterable<S> iterable);

    @Override
    User findOne (Long aLong);

    @Override
    boolean exists (Long aLong);

    @Override
    Iterable<User> findAll ();

    @Override
    Iterable<User> findAll (Iterable<Long> iterable);

    @Override
    long count ();

    @Override
    void delete (Long aLong);

    @Override
    void delete (User user);

    @Override
    void delete (Iterable<? extends User> iterable);

    @Override
    void deleteAll ();
}
